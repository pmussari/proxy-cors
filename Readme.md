# Proxy CORS bypass

## Summary
The Node.js CORS Bypass project focuses on building a server-side solution to bypass Cross-Origin Resource Sharing (CORS) headers. CORS is a security mechanism implemented by web browsers to restrict cross-origin requests made by client-side scripts. This project leverages Node.js to create a custom server that acts as a proxy, intercepts requests, forwards them to the target server, modifies headers response, and returning back, allowing clients to access resources from different origins without encountering CORS-related errors.

## Key Features
- **Custom Server**: Implement a Node.js server to handle incoming requests and serve as a proxy using [Express](https://expressjs.com/en/api.html).
- **Forwarding Requests**: Forward the modified request to the destinated resource.
- **Response Handling**: Capture the response from the resource server and forward it back to the client.
- **Header Modification**: Modify necessary headers, including Access-Control-Allow-Origin, to allow cross-origin requests.
- **Error Handling**: Handle potential errors and provide appropriate error responses if CORS bypass is not possible or encounters issues.

## Additional Considerations
- **Security**: Implement appropriate security measures to ensure only authorized requests are processed using Basic Auth. Also, it is blocking to share credentials (cookies, authorization headers) in case of malicious scripts.
- **Configuration Options**: Provide configuration options for specifying allowed content types, headers, and relevant parameters.

## Leftovers
- **Logging and Monitoring**: Implement logging and monitoring mechanisms to track requests and identify potential issues.
- **Integration test**: Implement integration tests to perform validations in common use cases with error prone, such as Upstream/Downstream timeout, errors, etc.
- **Environment Configuration**: Keep configuration isolated to the project and provide a way to make it flexible and manageable on each environment

## Configure local environment
- Requisites
```
node >= 16
```

- Installation using `yarn`
```
yarn install
```

- Hardcoded definitions

To run testing on the Demo Site, should be added the following line in `/etc/hosts`
```
127.0.0.1 cdn.static.internal
```
If it is required to change port on the proxy should be changed in `config.json`
```
{
  "port": xxx
}
```
Also it is possible to update user and pass for Basic and use tls on the proxy

- Run proxy
With actively watching (development phase)
```
yarn start:dev
```
```
yarn start:proxy
```
- Run demo sites (2 different ports to check CROSS ORIGIN RESOURCES SHARING)
With actively watching (development phase)
```
yarn start:demo
http://localhost:8020/cors_image.html --with CORS errors
http://localhost:8020/corp_image.html --with CORP errors
http://cdn.static.internal:8021/cors_image.html --without CORS errors
http://cdn.static.internal:8020/corp_image.html --without CORP errors
```
*** Errors above should be solved starting and configuring the proxy in your browser ***

- Testing case with Cypress
```
yarn test:cors-noproxy --should run the test cases without proxy
yarn test:cors-proxy --should run the test cases with proxy
```

## Benchmark

For checking and evaluating proxy performance against no using it, it was used [Apache Benchmark](https://httpd.apache.org/docs/trunk/programs/ab.html).
- How to run it 

ex: 5 concurrent requests and 20 requests in total
```
ab -c 5 -n 20 -P proxy_user:proxy_pass -X localhost:8009 https://urltoimage
ab -c 5 -n 20 https://urltoimage
```

In case of HTTP reaching `localhost`, its performance is degraded comparing to no using it in a 20%, but this case it could be ignored as it is adding a new layer of HTTP connection that in the other ways is almost null.  
However, using HTTPS proxy hiting outside the local environment, its performance basically is not affected, almost the same time to perform same request using or not using it.

