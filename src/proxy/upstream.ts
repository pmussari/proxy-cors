import { NextFunction, Request, RequestHandler, Response } from 'express'
import http from 'http'
import https from 'https'

const requester = (req: Request) => req.secure ? https.request : http.request

const upstream: RequestHandler = (req: Request, res: Response, next: NextFunction) => {
  console.log(`Proxy handle ${req.originalUrl}`)
  const request = requester(req)(req.originalUrl, {
    method: req.method,
    headers: req.headers
  }, (response) => {
    console.log(`Proxy upstream dispatched`)
    res.locals.upstreamResponse = response
    next()
  }).on('error', (err) => {
    next(err)
  })
  console.log(`Proxy sending data to upstream`)
  req.pipe(request).on('end', () => {
    request.end()
    console.log(`Proxy closing upstream request`)
  }).on('error', (err) => {
    next(err)
  })
}

export default upstream