import { NextFunction, Request, RequestHandler, Response } from 'express'

const downstream: RequestHandler = (_req: Request, res: Response, next: NextFunction) => {
  const { upstreamResponse } = res.locals
  if (!res.headersSent) {
    res.writeHead(upstreamResponse.statusCode ?? 0, upstreamResponse.headers)
  }
  upstreamResponse.pipe(res)
  upstreamResponse.on('end', () => {
    res.end()
  }).on('error', (errorMessage: string | undefined) => {
    next(new Error(errorMessage))
  })
}

export default downstream