import { Handler } from 'express'
import upstream from './upstream'
import downstream from './downstream'

type ProxyMiddlewares = {
  upstream: Handler,
  downstream: Handler
}

const proxy: ProxyMiddlewares = {
  upstream,
  downstream
}

export default proxy