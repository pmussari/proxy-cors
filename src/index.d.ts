import 'express';

interface Locals {
  upstreamResponse?: Response;
}

declare module 'express' {
  export interface Response  {
    locals: Locals;
  }
}