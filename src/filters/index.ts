import { Handler, NextFunction, Request, Response } from 'express'
import { hasAllowedContentType } from './contentTypes'
import { hasAllowedMethods } from './methods'
import omit from '../utils/omit'

const removeCorpHeaders = [
  'cross-origin-resource-policy'
]
const addCorsHeaders = {
  ['access-control-allow-origin']: '*',
  ['access-control-allow-headers']: 'X-Requested-With',
  ['access-control-allow-methods']: 'HEAD, GET',
  // This is to prevent sharing credentials from unexpected domain
  ['access-control-allow-credentials']: 'false',
}

const filterCorsHeaders: Handler = (req: Request, res: Response, next: NextFunction) => {
  const { upstreamResponse } = res.locals
  if (upstreamResponse !== null) {
    if (hasAllowedMethods(req) && hasAllowedContentType(upstreamResponse.headers)) {
      const filteredHeaders = omit(removeCorpHeaders, upstreamResponse.headers)
      res.writeHead(upstreamResponse.statusCode ?? 500, {
        ...filteredHeaders,
        ...addCorsHeaders,
        ['x-powered-by']: 'Proxy Bypass CORS'
      })
      console.log(`Filtered ${req.originalUrl}`)
    }
    next()
  } else {
    next(new Error('Upstream not initialized'))
  }
}

export { filterCorsHeaders }