import { Request } from "express"

// This Array of allowed methods could be moved to a config
const ALLOWED_METHODS : string[] = ['GET']

const hasAllowedMethods = (req: Request) : boolean => {
  return ALLOWED_METHODS.includes(req.method)
}

export { hasAllowedMethods }