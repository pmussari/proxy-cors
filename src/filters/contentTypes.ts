const CONTENT_TYPE = 'content-type'

// This Array of allowed types could be moved to a config
const ALLOWED_CONTENT_TYPES : string[] = [
  'image/*',
  'text/css',
  'font/*'
]

const regMatch = (type: string) => (allowedType: string) => 
  new RegExp(allowedType, 'i').test(type)

const hasAllowedContentType = (headers: Record<string, string>) : boolean => {
  const contentType = headers[CONTENT_TYPE]
  return ALLOWED_CONTENT_TYPES.some(regMatch(contentType))
}

export { hasAllowedContentType }