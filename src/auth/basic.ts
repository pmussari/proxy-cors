import { Request, RequestHandler, Response } from "express";
import config from '../utils/config'

type BasicUser = {
  user: string,
  password: string
}

const parseUser = (header: string): BasicUser => {
  const [user, password] = Buffer.from(header.split(' ')[1], 'base64').toString().split(':');
  return { user, password }
}

const basicAuth: RequestHandler = (req: Request, res: Response, next: Function) => {
  console.log('Proxy authentication')
  if (!req.headers["proxy-authorization"]) {
    res.statusCode = 407
    res.statusMessage = 'Proxy Authentication Required';
    res.setHeader('Proxy-Authenticate', 'Basic realm="Proxy User/Password required"')
    next(new Error('Proxy Authentication Required'))
  } else {
    try{
      const { user, password } = parseUser(req.headers["proxy-authorization"])
      if (config.auth.user === user && config.auth.password === password) {
        next()
      } else {
        next(new Error('Invalid user and password'))
      }
    } catch (err) {
      res.statusCode = 401
      res.statusMessage = 'Unauthorized';
      next(new Error('Proxy Authentication Failed'))
    }
  }
}

export default basicAuth