import { Request, Response } from 'express'

export default (err: Error, _req: Request, res: Response) => {
  console.error(`Proxy Error: ${err.message}`, err)
  res.send({
    error: err.message
  })
}