import { Express } from 'express'
import config from '../utils/config'
import startHttpServer from './http'
import startHttpsServer from './https'
import { ProxyServer } from './types'

const startProxyServer = (app: Express) => {
  const server: ProxyServer = config.tls
    ? startHttpsServer(app, config.port)
    : startHttpServer(app, config.port)
  const shutdown = () => {
    server.shutdown()
    setTimeout(() => {
      console.error('Proxy Server has not been exited successfully')
      process.exit(1)
    }, 5000);
  }
  process.on('SIGTERM', shutdown)
  process.on('SIGINT', shutdown)  
}

export { startProxyServer }