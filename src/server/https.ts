import { Express } from 'express'
import https from 'https'
import fs from 'fs'
import { ProxyServer } from './types';

const startHttpsServer = (app: Express, port: number): ProxyServer => {
  const server = https.createServer({
    key: fs.readFileSync('key.pem'),
    cert: fs.readFileSync('cert.pem')
  }, app).listen(port, () => {
    console.log(`Proxy HTTPS is running on ${port}...`)
  }).on('error', (err) => {
    console.error(`Error on HTTPS server: ${err.message}`)
    process.exit(1)
  })
  return {
    shutdown: () => {
      server.close(() => {
        console.log(`Proxy HTTPS has been closed`)
        process.exit(0)
      })
    }
  }
}

export default startHttpsServer