import { Express } from 'express'
import { ProxyServer } from './types'

const startHttpServer = (app: Express, port: number): ProxyServer => {
  const server = app.listen(port , () => {
    console.log(`Proxy HTTP is running on ${port}...`)
  }).on('error', (err) => {
    console.error(`Error on Proxy HTTP: ${err.message}`)
    process.exit(1)
  })
  return {
    shutdown: () => {
      server.close(() => {
        console.log(`Proxy HTTP has been closed`)
        process.exit(0)
      })
    }
  }
}

export default startHttpServer