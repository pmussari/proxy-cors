import * as config from '../../config.json'

export type Config = {
  auth: { user: string, password: string },
  port: number,
  tls: boolean
}

export default <Config> config