
const omit = (toOmit:string[], object: Record<string, string>): Record<string, string> => {
  const result = { ...object }
  toOmit.forEach(prop => delete result[prop])
  return result
}

export default omit