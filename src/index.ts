import express, { Express } from 'express'
import { basicAuth } from './auth'
import errorHandler from './error/errorHandler'
import proxy from './proxy'
import { filterCorsHeaders } from './filters'
import { startProxyServer } from './server'

const app: Express = express()

// Can be extracted as an auth module with a selected strategy
// (ex: Basic or Token)
app.use(basicAuth)

// This block can be abstracted to a Proxy middleware
// it should receive a filter only for headers with
// limit access to only manipulate headers sent in the downstream response
app.use(proxy.upstream)
app.use(filterCorsHeaders)
app.use(proxy.downstream)

// Errors handling for middlewares
app.use(errorHandler)

// Starting Proxy Server
startProxyServer(app)

