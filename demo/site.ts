import express, { Express } from 'express'
const PORT_REMOTE1: number = 8020
const PORT_REMOTE2: number = 8021
const appRemote1: Express = express()
const appRemote2: Express = express()

appRemote1.use((_req, res, next) => {
  res.setHeader('Cross-Origin-Resource-Policy', 'same-origin')
  next()
})
appRemote1.use(express.static('demo'))
appRemote2.use((_req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', `localhost:${PORT_REMOTE2}`)
  next()
})
appRemote2.use(express.static('demo'))

const serverFakeRemote1 = appRemote1.listen(PORT_REMOTE1,
  () => console.log(`Fake Remote 1 Server running on ${PORT_REMOTE1}`))
const serverFakeRemote2 = appRemote2.listen(PORT_REMOTE2,
  () => console.log(`Fake Remote 2 Server running on ${PORT_REMOTE2}`))

const shutdown = async () => {
  await Promise.all([
    new Promise((resolve) => {
      serverFakeRemote1.close(() => resolve(true))
    }),
    new Promise((resolve) => {
      serverFakeRemote2.close(() => resolve(true))
    })
  ]);
  console.log('Demo sites have been closed')
  process.exit(0)
}

process.on('SIGTERM', shutdown)
process.on('SIGINT', shutdown)