describe('Validate media files were not loaded without Proxy', () => {
  it('should not load CORS images required', () => {
    cy.visit('http://localhost:8021/cors_image.html')
    cy.get('#image-cors')
      .should('be.visible')
      .and('have.prop', 'naturalWidth')
      .should('not.be.greaterThan', 0)
  })
  it('should not load CORP images required', () => {
    cy.visit('http://localhost:8021/corp_image.html')
    cy.get('#image-corp')
      .should('be.visible')
      .and('have.prop', 'naturalWidth')
      .should('not.be.greaterThan', 0)
  })
  it('should not load CORS styles and fonts required', () => {
    cy.visit('http://localhost:8021/cors_styles.html')
    cy.get('#styles-cors')
      .should('not.be.visible')
  })
})