describe('Validate media files were loaded using Proxy', () => {
  it('should show CORS images required', () => {
    cy.visit('http://localhost:8021/cors_image.html')
    cy.get('#image-cors')
      .should('be.visible')
      .and('have.prop', 'naturalWidth')
      .should('be.greaterThan', 0)
  })
  it('should show CORP images required', () => {
    cy.visit('http://localhost:8021/corp_image.html')
    cy.get('#image-corp')
      .should('be.visible')
      .and('have.prop', 'naturalWidth')
      .should('be.greaterThan', 0)
  })
  it('should load CORS styles and fonts required', () => {
    cy.visit('http://localhost:8021/cors_styles.html')
    cy.get('#styles-cors')
      .should('be.visible')
  })
})